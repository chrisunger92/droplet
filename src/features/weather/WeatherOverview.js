import React from 'react';
import {useSelector} from 'react-redux';
import styles from './Weather.module.css';
import {selectCity, selectLastUpdated, selectWeather} from "./weatherSlice";

export function WeatherOverview() {
    const city = useSelector(selectCity);
    const weather = useSelector(selectWeather);
    const lastUpdated = useSelector(selectLastUpdated);

    return (
        <div className={styles.weatherOverview}>
            {/*TODO replace this with custom icons*/}
            {weather.icon !== '' &&
            <img
                className={styles.weatherIconImgLarge}
                src={"http://openweathermap.org/img/wn/" + weather.icon + "@4x.png"}
                alt="icon"/>
            }
            <span className={styles.currentTemp}>
                {weather.temp ? weather.temp + '°C' : 'Search for your city'}
            </span>
            <div className={styles.weatherOverviewFooter}>
                <span className={styles.cityName}>{city}</span>
                <span className={styles.date}>
                    {lastUpdated ? new Date(lastUpdated).toLocaleString() : ''}
                </span>
            </div>
        </div>
    )
}
