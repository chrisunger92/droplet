import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './Weather.module.css';

export function WeatherIcon(props) {

    return (
        <div className={styles.weatherIcon}>
            <img src={props.image} alt="icon" className={styles.weatherIconImg}/>
            {props.name}<br/>
            <span className={styles.weatherIconValue}>{props.value} {props.unit}</span>
        </div>
    )
}
