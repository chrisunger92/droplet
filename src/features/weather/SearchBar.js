import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import styles from './Weather.module.css';
import {fetchWeather, setCity} from './weatherSlice';

export function SearchBar() {
    const dispatch = useDispatch();
    const [cityName, setCityName] = useState('');

    const onCityChanged = e => setCityName(e.target.value);

    const dispatchCityAndFetchWeather = () => {
        if (cityName !== '') {
            dispatch(setCity(cityName));
            dispatch(fetchWeather(cityName));
            setCityName('');
        }
    }

    const onSubmitCity = e => {
        if (e.keyCode === 13 && cityName !== '') {
            dispatchCityAndFetchWeather();
        }
    }

    return (
        <div className={styles.searchBar}>
            <input className={styles.searchBarInput}
                   placeholder="Your City"
                   value={cityName}
                   onChange={onCityChanged}
                   onKeyDown={onSubmitCity}
            />
            <button className={styles.searchButton}
                    onClick={dispatchCityAndFetchWeather}>
                Search
            </button>
        </div>
    )
}
