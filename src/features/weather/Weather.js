import React from 'react';
import {SearchBar} from "./SearchBar";
import styles from './Weather.module.css';
import {WeatherIcon} from "./WeatherIcon";
import {WeatherOverview} from "./WeatherOverview";

/* images */
import minIcon from '../../img/winter.png';
import maxIcon from '../../img/summer.png';
import humidityIcon from '../../img/hygrometer.png';
import windSpeedIcon from '../../img/windsock.png';
import pressureIcon from '../../img/pressure-gauge.png';
import {selectWeather} from "./weatherSlice";
import {useSelector} from "react-redux";

export function Weather() {
    const weatherData = useSelector(selectWeather);

    return (
        <div className={styles.weatherApp}>
            <SearchBar />
            <div className={styles.weather}>
                <WeatherOverview />
                <div className={styles.weatherIcons}>
                    <WeatherIcon name="Min" image={minIcon} value={weatherData.minTemp} unit={"°C"}/>
                    <WeatherIcon name="Max" image={maxIcon} value={weatherData.maxTemp} unit={"°C"}/>
                    <WeatherIcon name="Humidity" image={humidityIcon} value={weatherData.humidity} unit={"%"}/>
                    <WeatherIcon name="Wind Speed" image={windSpeedIcon} value={weatherData.windSpeed} unit={"m/s"}/>
                    <WeatherIcon name="Pressure" image={pressureIcon} value={weatherData.pressure} unit={"hPa"}/>
                </div>
            </div>
        </div>
    )
}
