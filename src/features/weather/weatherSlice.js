import {createSlice} from '@reduxjs/toolkit';
import fetch from 'cross-fetch'

export const weatherSlice = createSlice({
    name: 'weather',
    initialState: {
        apiState: {
            isFetching: false,
            doReload: false,
            lastUpdated: null
        },
        city: '',
        weatherData: {
            temp: 0,
            description: '',
            icon: '',
            windSpeed: 0,
            maxTemp: 0,
            minTemp: 0,
            humidity: 0,
            pressure: 0
        }
    },
    reducers: {
        setCity: (state, action) => {
            state.city = action.payload;
            state.apiState.doReload = true;
        },
        receiveWeather: (state, action) => {
            console.log("Received weather: " + action.payload);
            state.weatherData = action.payload;
            state.apiState.isFetching = false;
            state.apiState.lastUpdated = Date.now();
            state.apiState.doReload = false;
        },
        requestWeather: state => {
            state.apiState.isFetching = true;
        }
    },
});

export const {setCity, receiveWeather, requestWeather} = weatherSlice.actions;

export const fetchWeather = city => dispatch => {
    let api = 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&appid=751862dfba36ac3505fa286e05ce93d9';
    dispatch(requestWeather);
    fetch(api)
        .then(response => response.json())
        .then(data => {
            return {
                temp: data.main.feels_like,
                description: data.weather[0].description,
                icon: data.weather[0].icon,
                windSpeed: data.wind.speed,
                maxTemp: data.main.temp_max,
                minTemp: data.main.temp_min,
                humidity: data.main.humidity,
                pressure: data.main.pressure
            }
        })
        .then(weatherData => dispatch(receiveWeather(weatherData)));
};

export const selectCity = state => state.weather.city;
export const selectWeather = state => state.weather.weatherData;
export const selectApiState = state => state.weather.apiState;
export const selectLastUpdated = state => state.weather.apiState.lastUpdated;

export default weatherSlice.reducer;
