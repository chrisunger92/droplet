import React from 'react';
import './App.css';
import {Weather} from './features/weather/Weather';
import logo from './img/droplet_logo.png';

function App() {
    return (
        <div className="app">
            <header className="app-header">
                <img className="app-logo" src={logo} alt="Droplet Logo"/>
                Droplet
            </header>
            <Weather />
            <p>
                Get real time weather information of your location with <strong className="app-color">Droplet</strong>
            </p>
            <footer className="app-footer">
                Made with <strong className="app-color">❤</strong> by Chris Unger
            </footer>
        </div>
    );
}

export default App;
